import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService } from '../services/auth.service';
import { StorageService } from '../services/storage.service';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = 'HomePage';

  pages: Array<{title: string, component: string}>;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public auth: AuthService,
    public storage: StorageService
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Ordens de serviço', component: 'OrdemServicoPage' },
      { title: 'Usuários', component: 'UsuariosPage'},
      { title: 'Clientes', component: 'ClientesPage'},
      { title: 'Perfil', component: 'ProfilePage' },
      { title: 'Sair', component: ''}
    ];

  }

  teste(title: string) {
    let role = this.storage.getRole();
    if(role && role.perfil) {
      if((title == 'Usuários' && role.perfil == 'ADMIN') || title != 'Usuários')  {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page : {title: string, component: string}) {

    switch (page.title){
      case 'Sair':
        this.auth.logout();
        this.nav.setRoot('HomePage');
        break;
      
      default:
        this.nav.setRoot(page.component);
    }

  }
}
