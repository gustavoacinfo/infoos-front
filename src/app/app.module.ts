import { ClienteService } from './../services/domain/cliente.service';
import { StorageService } from './../services/storage.service';
import { AuthService } from './../services/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonMaskModule } from '@pluritech/ion-mask';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ErrorInterceptorProvider } from '../interceptors/error-interceptor';
import { UsuarioService } from '../services/domain/usuario.service';
import { AuthInterceptorProvider } from '../interceptors/auth-interceptor';
import { RecuperarPage } from '../pages/home/home';
import { AlterarSenhaPage, AlterarDadosPage } from '../pages/profile/profile';
import { AdicionarUsuarioPage, VisualizarDadosPage } from '../pages/usuarios/usuarios';
import { OrdemService } from '../services/domain/ordem.service';
import { AdicionarOrdemPage, BuscarClientesPage, AlterarOrdemPage } from '../pages/ordem-servico/ordem-servico';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { NovoClientePage, AlterarClientePage } from '../pages/clientes/clientes';

@NgModule({
  declarations: [
    MyApp,
    RecuperarPage,
    AlterarSenhaPage,
    AlterarDadosPage,
    AdicionarUsuarioPage,
    AdicionarOrdemPage,
    BuscarClientesPage,
    AlterarOrdemPage,
    NovoClientePage,
    AlterarClientePage,
    VisualizarDadosPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonMaskModule.forRoot(),
    BrMaskerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RecuperarPage,
    AlterarSenhaPage,
    AlterarDadosPage,
    AdicionarUsuarioPage,
    AdicionarOrdemPage,
    BuscarClientesPage,
    AlterarOrdemPage,
    NovoClientePage,
    AlterarClientePage,
    VisualizarDadosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},

    AuthInterceptorProvider,
    ErrorInterceptorProvider,
    AuthService, 
    StorageService,
    UsuarioService,
    OrdemService,
    RecuperarPage,
    AlterarSenhaPage,
    AlterarDadosPage,
    AdicionarUsuarioPage,
    AdicionarOrdemPage,
    BuscarClientesPage,
    ClienteService,
    AlterarOrdemPage,
    NovoClientePage,
    ClienteService,
    AlterarClientePage,
    VisualizarDadosPage
  ]
})
export class AppModule {}
