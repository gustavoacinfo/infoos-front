import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API_CONFIG } from "../../config/api.config";
import { Observable } from 'rxjs/Rx';
import { OrdemDTO } from '../../models/ordem.dto';

@Injectable()
export class OrdemService {

    constructor(public http: HttpClient){

    }

    findAll() : Observable <OrdemDTO[]> {
        return this.http.get<OrdemDTO[]>(`${API_CONFIG.baseUrl}/ordemdeservico`)
    }

    insert(obj: Object) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/ordemdeservico`,
            obj,
            {
                observe: 'response',
                responseType: 'text' 
            }
        );
    }

    alterarSituacao(idOrdem: string, tipo: string) {
        return this.http.put(
            `${API_CONFIG.baseUrl}/ordemdeservico/alterar-status/${idOrdem}?tipo=${tipo}`,{},
            {
                observe: 'response',
                responseType: 'text' 
            });
    }

    changeDados(ordem: Object) {
        return this.http.put(
            `${API_CONFIG.baseUrl}/ordemdeservico`,
            ordem,
            {
                observe: 'response',
                responseType: 'text'
            });
    }

}