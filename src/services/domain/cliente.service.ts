import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API_CONFIG } from "../../config/api.config";
import { Observable } from 'rxjs/Rx';
import { ClienteDTO } from '../../models/cliente.dto';

@Injectable()
export class ClienteService {

    constructor(public http: HttpClient){

    }

    findAll() : Observable <ClienteDTO[]> {
        return this.http.get<ClienteDTO[]>(`${API_CONFIG.baseUrl}/cliente`)
    }

    insert(obj : Object){
        return this.http.post(
            `${API_CONFIG.baseUrl}/cliente`,
            obj,
            {
                observe: 'response',
                responseType: 'text'
            }
        );
    }

    changeDados(cliente: ClienteDTO) {
        return this.http.put(
            `${API_CONFIG.baseUrl}/cliente`,
            cliente,
            {
                observe: 'response',
                responseType: 'text'
            });
    }
}