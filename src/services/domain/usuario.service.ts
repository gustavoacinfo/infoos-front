import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { UsuarioDTO } from "../../models/usuario.dto";
import { API_CONFIG } from "../../config/api.config";
import { StorageService } from "../storage.service";

@Injectable()
export class UsuarioService {

    constructor(public http: HttpClient, public storage: StorageService) {
    }

    findAll() : Observable <UsuarioDTO[]> {
        return this.http.get<UsuarioDTO[]>(`${API_CONFIG.baseUrl}/usuario`)
    }

    findByEmail(email: string) : Observable<UsuarioDTO> {
        return this.http.get<UsuarioDTO>(`${API_CONFIG.baseUrl}/usuario/email?value=${email}`);
    }

    insert(obj: Object) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/usuario`,
            obj,
            {
                observe: 'response',
                responseType: 'text' 
            }
        );
    }

    alterarSituacao(idUsuario: string) {
        return this.http.put(
            `${API_CONFIG.baseUrl}/usuario/status/${idUsuario}`,{},
            {
                observe: 'response',
                responseType: 'text' 
            });
    }

    changePassword(senhaAtual: string, novaSenha: string) {
        return this.http.put(
            `${API_CONFIG.baseUrl}/usuario/alterarSenha?senhaAtual=${senhaAtual}&novaSenha=${novaSenha}`,
            {},
            {
                observe: 'response',
                responseType: 'text'
            });
    }

    changeDados(usuario: UsuarioDTO) {
        return this.http.put(
            `${API_CONFIG.baseUrl}/usuario`,
            usuario,
            {
                observe: 'response',
                responseType: 'text'
            });
    }
}