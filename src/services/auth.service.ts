import { StorageService } from './storage.service';
import { LocalUser } from './../models/local_user';
import { HttpClient } from '@angular/common/http';
import { CredenciaisDTO } from './../models/credenciais.dto';
import { Injectable } from "@angular/core";
import { API_CONFIG } from '../config/api.config';
import { JwtHelper } from 'angular2-jwt';
import { EmailDTO } from '../models/email.dto';
import { UsuarioService } from './domain/usuario.service';
import { Role } from '../models/role';

@Injectable()
export class AuthService {

    jwtHelper: JwtHelper = new JwtHelper();

    constructor(public http: HttpClient, public storage: StorageService, public usuarioService: UsuarioService){

    }

    authentication(creds : CredenciaisDTO) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/login`, 
            creds,
            {
                observe: 'response',
                responseType: 'text'
            })
    }

    refreshToken() {
        return this.http.post(
            `${API_CONFIG.baseUrl}/auth/refresh_token`, 
            {},
            {
                observe: 'response',
                responseType: 'text'
            })
    }

    successfulllogin(authorizationValue : string){
        let tok = authorizationValue.substring(7);
        let user : LocalUser = {
            token : tok,
            email: this.jwtHelper.decodeToken(tok).sub
        };
        this.storage.setLocalUser(user);
        let localUser = this.storage.getLocalUser();
        if(localUser && localUser.email) {
            this.pegarRole(user.email);
        }
    }

    pegarRole(email: string) {
        this.usuarioService.findByEmail(email)
            .subscribe(response => {
                let role : Role = {
                    perfil : response.perfis[0]
                };
                this.storage.setRole(role);
        });
        //tratar erro
    }

    logout(){
        this.storage.setLocalUser(null);
        this.storage.setRole(null);
    }

    forgot(email: EmailDTO){
        return this.http.post(
            `${API_CONFIG.baseUrl}/auth/forgot`, 
            email,
            {
                observe: 'response',
                responseType: 'text'
            })
    }

  
}