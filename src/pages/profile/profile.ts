import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { StorageService } from '../../services/storage.service';
import { UsuarioDTO } from '../../models/usuario.dto';
import { UsuarioService } from '../../services/domain/usuario.service';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  usuario: UsuarioDTO;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public storage: StorageService,
    public usuarioService: UsuarioService,
    public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    let localUser = this.storage.getLocalUser();
    if(localUser && localUser.email) {
      this.usuarioService.findByEmail(localUser.email)
        .subscribe(response => {
          this.usuario = response;
        },
        error => {
          if (error.status == 403) {
            this.navCtrl.setRoot('HomePage');
          }
        });
    } else {
      this.navCtrl.setRoot('HomePage');
    }
  }

  alterarSenha() {
    let modal = this.modalCtrl.create(AlterarSenhaPage);
    modal.present();
  }

  alterarDados() {
    let modal = this.modalCtrl.create(AlterarDadosPage, {'val': this.usuario});
    modal.present();
  }

}

@Component({
  selector: 'alterarSenha-home',
  templateUrl: 'alterarSenha.html'
})
export class AlterarSenhaPage {

  obj = {
    senhaAtual: "",
    novaSenha: "",
    confirmacaoSenha: ""
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public usuarioService: UsuarioService,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
  }

  alterar() {
    const loader = this.loadingCtrl.create({
      content: "Alterando senha..."
    });
    loader.present();
    this.usuarioService.changePassword(this.obj.senhaAtual, this.obj.novaSenha)
      .subscribe(response => {
        loader.dismiss();
        switch(response.status) {
          case 200:
              this.handle200();
            break;
        }
      }, 
      error => {
        loader.dismiss();
      });
  }

  handle200() {
    let alert = this.alertCtrl.create({
        title: 'Sucesso!',
        message: 'Sua senha foi alterada com sucesso!',
        enableBackdropDismiss: false,
        buttons: [
            {
                text: 'Ok',
                handler: () => {
                  this.dismiss();
                }
            }
        ]
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}


@Component({
  selector: 'alterarDados-home',
  templateUrl: 'alterarDados.html'
})
export class AlterarDadosPage {

  usuario: UsuarioDTO;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public usuarioService: UsuarioService,
    public storage: StorageService,
    public loadingCtrl: LoadingController) {
      console.log(navParams.get('val'));
      this.usuario = navParams.get('val');
      
  }

  alterar() {
    const loader = this.loadingCtrl.create({
      content: "Alterando dados..."
    });
    loader.present();
    delete this.usuario.perfis;
    this.usuarioService.changeDados(this.usuario)
      .subscribe(response => {
        loader.dismiss();
        switch(response.status) {
          case 200:
              this.handle200();
            break;
        }
      }, 
      error => {
        loader.dismiss();
      });
  }

  handle200() {
    let alert = this.alertCtrl.create({
        title: 'Dados alterados',
        message: 'Seus dados foram alterados com sucesso!',
        enableBackdropDismiss: false,
        buttons: [
            {
                text: 'Ok',
                handler: () => {
                  this.dismiss();
                }
            }
        ]
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
