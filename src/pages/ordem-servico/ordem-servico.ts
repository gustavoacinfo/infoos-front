import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, ViewController, Nav, LoadingController } from 'ionic-angular';
import { OrdemService } from '../../services/domain/ordem.service';
import { OrdemDTO } from '../../models/ordem.dto';
import { ClienteService } from '../../services/domain/cliente.service';
import { ClienteDTO } from '../../models/cliente.dto';

/**
 * Generated class for the OrdemServicoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ordem-servico',
  templateUrl: 'ordem-servico.html',
})
export class OrdemServicoPage {

  items: OrdemDTO[];
  itemsFilter: OrdemDTO[];
  teste: OrdemDTO[];
  testRadioOpen: boolean;
  testRadioResult;
  filtro: string = 'serviço';
  situacao: string = 'todas';
  busca: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public ordemService: OrdemService,
    public modalCtrl: ModalController,
    public alerCtrl: AlertController) {
  }

  ionViewDidLoad() {
    this.ordemService.findAll()
      .subscribe(response => {
        this.items = response;
        this.initializeItems();
      }, 
      error => {}
      );
  }

  initializeItems() {
    this.itemsFilter = this.items;
  }


  filtrarPorSituacao() {
    this.initializeItems();
    if(this.situacao == 'todas') {
      this.itemsFilter;
    } else if(this.situacao == 'aberta') {
      this.itemsFilter = this.itemsFilter.filter((item) => {
        return (item.status == '1');
      })
    } else if(this.situacao == 'andamento') {
      this.itemsFilter = this.itemsFilter.filter((item) => {
        return (item.status == '2');
      })
    } else { //encerrada
      this.itemsFilter = this.itemsFilter.filter((item) => {
        return (item.status == '3');
      })
    }
    if(this.busca != undefined || this.busca != '') {
      this.getItemsNew(this.busca);
    }
  }

  getItems(ev: any) {
    this.filtrarPorSituacao();
    const val = ev.target.value;
    if (val && val.trim() != '') {
      if(this.filtro == 'serviço') {
        this.itemsFilter = this.itemsFilter.filter((item) => {
          return (item.servicoRealizado.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else if (this.filtro == 'cliente') {
        this.itemsFilter = this.itemsFilter.filter((item) => {
          return (item.cliente.pessoa.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else { //Responsável
        this.itemsFilter = this.itemsFilter.filter((item) => {
          return (item.usuario.pessoa.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }
  }

  getItemsNew(val: string) {
    if (val && val.trim() != '') {
      if(this.filtro == 'serviço') {
        this.itemsFilter = this.itemsFilter.filter((item) => {
          return (item.servicoRealizado.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else if (this.filtro == 'cliente') {
        this.itemsFilter = this.itemsFilter.filter((item) => {
          return (item.cliente.pessoa.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else { //Responsável
        this.itemsFilter = this.itemsFilter.filter((item) => {
          return (item.usuario.pessoa.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }
  }

  adicionarOrdem() {
    let modal = this.modalCtrl.create(AdicionarOrdemPage);
    modal.present();
  }

  alterarSituacao(idOrdem: string, valor: string) {
    let alert = this.alerCtrl.create();
    alert.setTitle('Alterar situação');

    if(valor != "1") {
      alert.addInput({
        type: 'radio',
        label: 'Aberta',
        value: '1'
      });
    }

    if(valor != "2") {
      alert.addInput({
        type: 'radio',
        label: 'Em andamento',
        value: '2'
      });
    }

    if(valor != "3") {
      alert.addInput({
        type: 'radio',
        label: 'Encerrada',
        value: '3'
      });
    }

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Radio data:', data);
        this.testRadioOpen = false;
        this.testRadioResult = data;
        if (data != undefined) {
          if(data == '1') {
            this.confirmarAlteracaoParaAberta(idOrdem, data);
          } else if(data == '2') {
            this.confirmarAlteracaoParaAndamento(idOrdem, data);
          } else {
            this.confirmarAlteracaoParaEncerrada(idOrdem, data);
          }
        }
      }
    });

    alert.present().then(() => {
      this.testRadioOpen = true;
    });
  }

  confirmarAlteracaoParaAberta(idOrdem: string, tipo: string) {
    const confirm = this.alerCtrl.create({
      title: 'Confirmação!',
      message: "Deseja realmente alterar a situação para <b>aberta</b>?",
      buttons: [
        {
          text: 'Cancelar', handler: () => {}
        },
        {
          text: 'Ok',
          handler: () => {
            this.enviarAlteracaoSituacao(idOrdem, tipo);
          }
        }
      ]
    });
    confirm.present();
  }

  confirmarAlteracaoParaAndamento(idOrdem: string, tipo: string) {
    const confirm = this.alerCtrl.create({
      title: 'Confirmação!',
      message: "Deseja realmente alterar a situação para <b>em andamento</b>?",
      buttons: [
        {
          text: 'Cancelar', handler: () => {}
        },
        {
          text: 'Ok',
          handler: () => {
            this.enviarAlteracaoSituacao(idOrdem, tipo);
          }
        }
      ]
    });
    confirm.present();
  }

  confirmarAlteracaoParaEncerrada(idOrdem: string, tipo: string) {
    const confirm = this.alerCtrl.create({
      title: 'Confirmação!',
      message: "Deseja realmente alterar a situação para <b>encerrada</b>? Caso altere, não será mais possível alterar os dados da ordem e nem a sua situação.</ng-container>",
      buttons: [
        {
          text: 'Cancelar', handler: () => {}
        },
        {
          text: 'Ok',
          handler: () => {
            this.enviarAlteracaoSituacao(idOrdem, tipo);
          }
        }
      ]
    });
    confirm.present();
  }

  enviarAlteracaoSituacao(idOrdem: string, tipo: string) {
    this.ordemService.alterarSituacao(idOrdem, tipo)
      .subscribe(response => {
        switch(response.status) {
          case 200:
            this.handle200();
            break;
        }
      });
  }

  handle200() {
    let alert = this.alerCtrl.create({
        title: 'Situação alterada com sucesso',
        enableBackdropDismiss: false,
        buttons: [
            {
                text: 'Ok',
                handler: data => {
                  this.navCtrl.setRoot('OrdemServicoPage');
                }
            }
        ]
    });
    alert.present();
  }

  alterarOrdem(ordem: OrdemDTO) {
    // if(ordem.status != "3") {
      console.log('ok');
      let modal = this.modalCtrl.create(AlterarOrdemPage, {'val': ordem});
      modal.present();
    // }
  }

}


@Component({
  selector: 'adicionarOrdem-ordem-servico',
  templateUrl: 'adicionarOrdem.html'
})
export class AdicionarOrdemPage {
  @ViewChild(Nav) nav: Nav;

  nomeCliente : string;
  cliente: ClienteDTO;
  ordem = {
    dataInicio: "",
    dataFinal: "",
    equipamento : "",
    modelo : "",
    marca : "",
    defeitoInformado : "",
    servicoRealizado : "",
    observacao : "",
    valorProdutos : "",
    valorMaoDeObra : "",
    cliente: {
        id: ""
    },
    usuario: {
      perfis: []
    }
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public ordemService: OrdemService,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController) {
      
  }

  ionViewWillEnter(){
    if(this.navParams.get('name')) {
      this.cliente = this.navParams.get('name');
      this.ordem.cliente.id = this.cliente.id;
      this.nomeCliente = this.cliente.pessoa.nome;
      console.log(this.cliente.pessoa.nome)
    }
  }

  salvarOrdem() {
    // delete this.cliente.pessoa;
    const loader = this.loadingCtrl.create({
      content: "Cadastrando ordem..."
    });
    loader.present();
    this.ordemService.insert(this.ordem)
      .subscribe(response => {
        loader.dismiss();
        switch(response.status) {
          case 201:
              this.handle201();
            break;
        }
      },
      error => {
        loader.dismiss();
      });
  }

  handle201() {
    let alert = this.alertCtrl.create({
        title: 'Sucesso!',
        message: 'Cadastro efetuado com sucesso.',
        enableBackdropDismiss: false,
        buttons: [
            {
                text: 'Ok',
                handler: data => {
                  this.navCtrl.setRoot('OrdemServicoPage');
                }
            }
        ]
    });
    alert.present();
  }

  abrirModalClientes() {
    let modal = this.modalCtrl.create(BuscarClientesPage);
    modal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}


@Component({
  selector: 'buscarClientes-ordem-servico',
  templateUrl: 'buscarClientes.html'
})
export class BuscarClientesPage {

  items: ClienteDTO[];
  itemsFilter: ClienteDTO[];
  itemSelecionado: ClienteDTO;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public ordemService: OrdemService,
    public clienteService: ClienteService) {
  }

  ionViewDidLoad() {
    this.clienteService.findAll()
      .subscribe(response => {
        this.items = response;
        this.initializeItems();
      }, 
      error => {}
      );
  }

  initializeItems() {
    this.itemsFilter = this.items;
  }

  getItems(ev: any) {
    this.initializeItems();
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.itemsFilter = this.itemsFilter.filter((item) => {
        return (item.pessoa.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  clienteSelecionado(item: ClienteDTO) {
    this.navCtrl.getPrevious().data.name = item;
    this.navCtrl.pop();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}



@Component({
  selector: 'alterarOrdem-ordem-servico',
  templateUrl: 'alterarOrdem.html'
})
export class AlterarOrdemPage {

  ordem = {
    dataInicio: "",
    dataFinal: "",
    equipamento : "",
    modelo : "",
    marca : "",
    defeitoInformado : "",
    servicoRealizado : "",
    observacao : "",
    valorProdutos : "",
    valorMaoDeObra : "",
    cliente: {
        id: ""
    },
    usuario: {
      perfis: []
    }
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public ordemService: OrdemService,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController) {
      console.log(navParams.get('val'));
      this.ordem = navParams.get('val');
  }

  ionViewWillEnter(){
  }

  alterar() {
    const loader = this.loadingCtrl.create({
      content: "Alterando ordem..."
    });
    loader.present();
    delete this.ordem.usuario.perfis;
    this.ordemService.changeDados(this.ordem)
      .subscribe(response => {
        loader.dismiss();
        switch(response.status) {
          case 200:
              this.handle200();
            break;
        }
      }, 
      error => {
        loader.dismiss();
      });
  }

  handle200() {
    let alert = this.alertCtrl.create({
        title: 'Dados alterados',
        message: 'Seus dados foram alterados com sucesso!',
        enableBackdropDismiss: false,
        buttons: [
            {
                text: 'Ok',
                handler: () => {
                  this.dismiss();
                }
            }
        ]
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}


