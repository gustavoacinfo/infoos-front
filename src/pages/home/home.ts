import { AuthService } from './../../services/auth.service';
import { CredenciaisDTO } from './../../models/credenciais.dto';
import { Component } from '@angular/core';
import { NavController, MenuController, ModalController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { IonicPage } from 'ionic-angular/navigation/ionic-page';
import { EmailDTO } from '../../models/email.dto';

@IonicPage()

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  creds : CredenciaisDTO = {
      email: "",
      senha: ""
  };

  constructor(
    public navCtrl: NavController, 
    public menu: MenuController,
    public auth: AuthService,
    public modalCtrl: ModalController
  ) {
  }

  ionViewWillEnter() {
    this.menu.swipeEnable(false);
    }

  ionViewDidLeave() {
    this.menu.swipeEnable(true);
  }

  ionViewDidEnter() {
    this.auth.refreshToken()
      .subscribe(response => {
        this.auth.successfulllogin(response.headers.get('Authorization'));
        this.navCtrl.setRoot('OrdemServicoPage');
      }, 
      error => {});
  }

  openModal() {
    let modal = this.modalCtrl.create(RecuperarPage);
    modal.present();
  }

  login(){
    this.auth.authentication(this.creds)
      .subscribe(response => {
        this.auth.successfulllogin(response.headers.get('Authorization'));
        this.navCtrl.setRoot('OrdemServicoPage');
      }, 
      error => {});
  }

  cadastrarUsuario(){
    this.navCtrl.push('UsuariosPage');
  }
}

@Component({
  selector: 'recuperar-home',
  templateUrl: 'recuperar.html'
})
export class RecuperarPage {

  obj : EmailDTO = {
    email: ""
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public auth: AuthService,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
  }

  forgot(){
    const loader = this.loadingCtrl.create({
      content: "Enviando nova senha..."
    });
    loader.present();
    this.auth.forgot(this.obj)
      .subscribe(response => {
        loader.dismiss();
        switch(response.status) {
          case 204:
            this.handle204();
            break;

        }
      }, 
      error => {
        loader.dismiss();
      });
  }

  handle204() {
    let alert = this.alertCtrl.create({
        title: 'Sucesso!',
        message: 'Foi enviada uma nova senha para o e-mail informado.',
        enableBackdropDismiss: false,
        buttons: [
            {
                text: 'Ok'
            }
        ]
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
