import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, ViewController, LoadingController } from 'ionic-angular';
import { ClienteService } from '../../services/domain/cliente.service';
import { ClienteDTO } from '../../models/cliente.dto';

/**
 * Generated class for the ProdutosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-clientes',
  templateUrl: 'clientes.html',
})
export class ClientesPage {

  items: ClienteDTO[];
  itemsFilter: ClienteDTO[];
  cliente: ClienteDTO;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public clienteService: ClienteService,
    public modalCtrl: ModalController
  ) {
  }

  ionViewDidLoad() {
    this.clienteService.findAll()
      .subscribe(response => {
        this.items = response;
        this.initializeItems();
      }, 
      error => {}
      );
  }

  initializeItems() {
    this.itemsFilter = this.items;
  }

  getItems(ev: any) {
    this.initializeItems();
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.itemsFilter = this.itemsFilter.filter((item) => {
        return (item.pessoa.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  alterarDados(obj : Object) {
    let modal = this.modalCtrl.create(AlterarClientePage, {obj});
    modal.present();
  }

  openModal() {
    let modal = this.modalCtrl.create(NovoClientePage);
    modal.present();
  }

}

@Component({
  selector: 'novo-cliente',
  templateUrl: 'novocliente.html',
})

export class NovoClientePage{
  
  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";
  pureResult: any;
  maskedId: any;
  val: any;
  v: any;

  cliente = {
    documento:"",
    tipoCliente:"",
    pessoa:{
      nome: "",
      telefone: "",
      sexo: "",
      dataNasc: "",
      endereco: {
        bairro: "",
        cep: "",
        complemento: "",
        endereco: "",
        numero: ""
      }
    }
  }

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public clienteService: ClienteService,
              public viewCtrl: ViewController,
              public alertCrtl: AlertController,
              public loadingCtrl: LoadingController) {

  }

  salvarCliente(){
    const loader = this.loadingCtrl.create({
      content: "Cadastrando cliente..."
    });
    loader.present();
     this.clienteService.insert(this.cliente)
       .subscribe(response => {
          loader.dismiss();
          this.showInsertOk();
       },
      error => {
        loader.dismiss();
      });
  }

  showInsertOk(){
    let alert = this.alertCrtl.create({
      title: 'Sucesso!',
      message: 'Cadastro efetuado com sucesso.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
            this.navCtrl.setRoot(ClientesPage);
          }
        }
      ]
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  format(valString) {
    if (!valString) {
        return '';
    }
    let val = valString.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
    this.pureResult = parts;
    if(parts[0].length <= 11){
      this.maskedId = this.cpf_mask(parts[0]);
      return this.maskedId;
    }else{
      this.maskedId = this.cnpj(parts[0]);
      return this.maskedId;
    }
};

unFormat(val) {
    if (!val) {
        return '';
    }
    val = val.replace(/\D/g, '');

    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
};

 cpf_mask(v) {
    v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
    v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
    v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
    //de novo (para o segundo bloco de números)
    v = v.replace(/(\d{3})(\d{1,2})$/, '$1-$2'); //Coloca um hífen entre o terceiro e o quarto dígitos
    return v;
}

 cnpj(v) {
    v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3'); //Coloca ponto entre o quinto e o sexto dígitos
    v = v.replace(/\.(\d{3})(\d)/, '.$1/$2'); //Coloca uma barra entre o oitavo e o nono dígitos
    v = v.replace(/(\d{4})(\d)/, '$1-$2'); //Coloca um hífen depois do bloco de quatro dígitos
    return v;
}
  
}

@Component({
  selector: 'alterarCliente-home',
  templateUrl: 'alterarCliente.html'
})
export class AlterarClientePage {

    DECIMAL_SEPARATOR=".";
    GROUP_SEPARATOR=",";
    pureResult: any;
    maskedId: any;
    val: any;
    v: any;
  
    cliente: ClienteDTO;
  
    constructor(
      public navCtrl: NavController, 
      public navParams: NavParams,
      public alertCtrl: AlertController,
      public viewCtrl: ViewController,
      public clienteService: ClienteService,
      public loadingCtrl: LoadingController
      ){
          
          this.cliente = navParams.data.obj;
      } 
    
  
    alterar() {
      const loader = this.loadingCtrl.create({
        content: "Alterando cliente..."
      });
      loader.present();
      this.clienteService.changeDados(this.cliente)
        .subscribe(response => {
          loader.dismiss();
          switch(response.status) {
            case 200:
                this.handle200();
              break;
          }
        }, 
        error => {
          loader.dismiss();
        });
    }
  
    handle200() {
      let alert = this.alertCtrl.create({
          title: 'Dados alterados',
          message: 'Seus dados foram alterados com sucesso!',
          enableBackdropDismiss: false,
          buttons: [
              {
                  text: 'Ok',
                  handler: () => {
                    this.dismiss();
                  }
              }
          ]
      });
      alert.present();
    }
  
    dismiss() {
      this.viewCtrl.dismiss();
    }

    format(valString) {
      if (!valString) {
          return '';
      }
      let val = valString.toString();
      const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
      this.pureResult = parts;
      if(parts[0].length <= 11){
        this.maskedId = this.cpf_mask(parts[0]);
        return this.maskedId;
      }else{
        this.maskedId = this.cnpj(parts[0]);
        return this.maskedId;
      }
  };
  
  unFormat(val) {
      if (!val) {
          return '';
      }
      val = val.replace(/\D/g, '');
  
      if (this.GROUP_SEPARATOR === ',') {
          return val.replace(/,/g, '');
      } else {
          return val.replace(/\./g, '');
      }
  };
  
    cpf_mask(v) {
      v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
      v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
      v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
      //de novo (para o segundo bloco de números)
      v = v.replace(/(\d{3})(\d{1,2})$/, '$1-$2'); //Coloca um hífen entre o terceiro e o quarto dígitos
      return v;
  }
  
    cnpj(v) {
      v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
      v = v.replace(/^(\d{2})(\d)/, '$1.$2'); //Coloca ponto entre o segundo e o terceiro dígitos
      v = v.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3'); //Coloca ponto entre o quinto e o sexto dígitos
      v = v.replace(/\.(\d{3})(\d)/, '.$1/$2'); //Coloca uma barra entre o oitavo e o nono dígitos
      v = v.replace(/(\d{4})(\d)/, '$1-$2'); //Coloca um hífen depois do bloco de quatro dígitos
      return v;
  }
}
