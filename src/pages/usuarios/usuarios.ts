import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, ViewController, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UsuarioService } from '../../services/domain/usuario.service';
import { UsuarioDTO } from '../../models/usuario.dto';



@IonicPage()
@Component({
  selector: 'page-usuarios',
  templateUrl: 'usuarios.html',
})
export class UsuariosPage {

  items: UsuarioDTO[];
  itemsFilter: UsuarioDTO[];
  formGroup: FormGroup;
  searchQuery: string = '';
  usuario: UsuarioDTO;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public usuarioService: UsuarioService,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {

  }

  ionViewDidLoad() {
    this.usuarioService.findAll()
      .subscribe(response => {
        this.items = response;
        this.initializeItems();
      }, 
      error => {}
      );
    
  }

  initializeItems() {
    this.itemsFilter = this.items;
  }

  adicionarUsuario() {
    let modal = this.modalCtrl.create(AdicionarUsuarioPage);
    modal.present();
  }

  getItems(ev: any) {
    this.initializeItems();
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.itemsFilter = this.itemsFilter.filter((item) => {
        return (item.pessoa.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  visualizarDados(obj: UsuarioDTO) {
    console.log('teste');
    let modal = this.modalCtrl.create(VisualizarDadosPage, {'val': obj});
    modal.present();
  }

  alterarSituacaoUsuario(usuario : UsuarioDTO) {
    this.usuarioService.alterarSituacao(usuario.id)
      .subscribe(response => {
        switch(response.status) {
          case 200:
            if(usuario.ativo == '1') {
              this.handle200Inativo(usuario);
            } else {
              this.handle200Ativo(usuario);
            }
            break;
          default:
            this.default();
        }
      });
  }

  handle200Inativo(usuario : UsuarioDTO) {
    let alert = this.alertCtrl.create({
        title: 'Sucesso!',
        message: 'O usuário ' + usuario.pessoa.nome + ' está <b>inativo</b>, não podendo mais acessar o aplicativo.',
        enableBackdropDismiss: false,
        buttons: [
            {
                text: 'Ok',
                handler: data => {
                  // location.reload();
                  // this.navCtrl.setRoot('UsuariosPage');
                  this.ionViewDidLoad()
                }
            }
        ]
    });
    alert.present();
  }

  handle200Ativo(usuario : UsuarioDTO) {
    let alert = this.alertCtrl.create({
        title: 'Sucesso!',
        message: 'O usuário ' + usuario.pessoa.nome + ' está <b>ativo</b>, podendo acessar o aplicativo.',
        enableBackdropDismiss: false,
        buttons: [
            {
                text: 'Ok',
                handler: data => {
                  // location.reload();
                  // this.navCtrl.setRoot('UsuariosPage');
                  this.ionViewDidLoad()
                }
            }
        ]
    });
    alert.present();
  }

  default() {
    let alert = this.alertCtrl.create({
        title: 'Não foi possível alterar a situação',
        enableBackdropDismiss: false,
        buttons: [
            {
                text: 'Ok',
                handler: data => {
                  this.ionViewDidLoad()
                }
            }
        ]
    });
    alert.present();
  }

}

@Component({
  selector: 'adicionarUsuario-home',
  templateUrl: 'adicionarUsuario.html'
})
export class AdicionarUsuarioPage {

  usuario = {
    cpf: "136.985.926-06",
    email: "leobarros.evangelista@gmail.com",
    pessoa: {
        nome: "Leandro",
        telefone: "(38) 99999999",
        sexo: "Masculino",
        dataNasc: "10/10/1998",
        endereco: {
            bairro: "centro",
            cep: "38680-000",
            complemento: "casa",
            endereco: "asdfs",
            numero: "10"
        }
    }
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public usuarioService: UsuarioService,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
  }

  salvarUsuario() {
    const loader = this.loadingCtrl.create({
      content: "Cadastrando usuário..."
    });
    loader.present();
    this.usuarioService.insert(this.usuario)
      .subscribe(response => {
        loader.dismiss();
        switch(response.status) {
          case 201:
              this.handle201();
            break;
            
        }
      },
      error => {
        loader.dismiss();
      });
  }

  handle201() {
    let alert = this.alertCtrl.create({
        title: 'Sucesso!',
        message: 'Cadastro efetuado com sucesso. A senha do novo usuário foi enviada para o e-mail informado no cadastro.',
        enableBackdropDismiss: false,
        buttons: [
            {
                text: 'Ok',
                handler: data => {
                  this.navCtrl.setRoot('UsuariosPage');
                }
            }
        ]
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}

@Component({
  selector: 'dadosUsuario-home',
  templateUrl: 'dadosUsuario.html'
})
export class VisualizarDadosPage {

  usuario : UsuarioDTO;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public usuarioService: UsuarioService) {
      this.usuario = navParams.get('val');
  }

  ionViewDidLoad() {
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}