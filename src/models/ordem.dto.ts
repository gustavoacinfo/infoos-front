export interface OrdemDTO{
    id : string;
    status : string;
    dataInicio : string;
    dataFinal : string;
    equipamento : string;
    modelo : string;
    marca : string;
    defeitoInformado : string;
    servicoRealizado : string;
    observacao : string;
    valorProdutos : string;
    valorMaoDeObra : string;
    cliente : {
        id : string;
        pessoa : {
            nome : string;
        }
    };
    usuario : {
        id : string;
        pessoa: {
            nome : string;
        }
    }
}