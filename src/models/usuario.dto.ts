export interface UsuarioDTO {
    id: string;
    cpf: string;
    email: string;
    ativo: string;
    pessoa: {
        id: string;
        nome: string;
        telefone: string;
        sexo: string;
        dataNasc: string;
        endereco: {
            id: string;
            bairro: string;
            cep: string;
            complemento: string;
            endereco: string;
            numero: string;
        }
    },
    perfis: Array<any>;
}