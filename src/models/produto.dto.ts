export interface ProdutoDTO{
    id : string;
    descricao : string;
    marca: string;
    fornecedor: string;
    preco: number;
    quantidade: number;
}