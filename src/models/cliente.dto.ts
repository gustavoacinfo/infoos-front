export interface ClienteDTO {
    id: string;
    documento: string;
    tipoCliente: string;
    tipoempresa: string;
    pessoa: {
        id: string;
        nome: string;
        telefone: string;
        sexo: string;
        dataNasc: string;
        endereco: {
            id: string;
            bairro: string;
            cep: string;
            complemento: string;
            endereco: string;
            numero: string;
        }
    }
}